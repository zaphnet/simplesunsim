package main

import (
	"fmt"
	"time"

	"github.com/perolo/excellogger"
)

const timeslice = 1 * time.Minute
const waterspec = 4180.0 //kJ/l
const area = 7.32        //7.32 Solpanel

const radiatorflow = 1500.0 / 3600 // l/s

// Radiatorkretsflöde typ intervallet 1000 - 2000 liter/h
var headers []string

//var timeser = []float64{0.0, 7.0, 12.0, 18.0, 24.0}
//var lutpower = []float64{0, 0, 600, 0, 0}

// 13th April file:///C:/Users/pereo/My%20Drive/Yocreo/SPrapp2002-20.pdf
var timeser = []float64{400 / 60, 450 / 60, 500 / 60, 575 / 60, 750 / 60, 875 / 60, 950 / 60, 1050 / 60, 1100 / 60}
var lutpower = []float64{0, 120, 240, 480, 570, 480, 360, 120, 0}

func lininterp1d(axis []float64, mapa []float64, x float64) (y float64) {
	y = 0.0
	if x <= axis[0] {
		y = mapa[0]
	} else if x >= axis[len(axis)-1] {
		y = mapa[len(axis)-1]
	} else {
		for i := range axis {
			if x == axis[i] {
				y = mapa[i]
				break
			} else if (x > (axis[i])) && (x < (axis[i+1])) {
				x1 := axis[i]
				x2 := axis[i+1]
				y1 := mapa[i]
				y2 := mapa[i+1]
				slope := (y2 - y1) / (x2 - x1)
				y = y1 + slope*(x-x1)
				break
			}
		}
	}
	return y
}

func main() {

	deltaEnergy := 0.0
	targetTemp := 60.0
	radiatorReturnTemp := 0.0
	currentPower := 0.0                    //W
	oldTemp := -2.0                        //C
	var newTemp float64                    //C
	currentFlow := 1 / timeslice.Seconds() // l/s
	adjFlow := 0.0
	start, _ := time.Parse("2006/01/02", "2022/04/02")
	theTime := start
	end := start.Add(time.Hour * 24)

	file := fmt.Sprintf("C:/Temp/Report%s.xlsx", time.Now().Format("15-04-05"))

	excellogger.NewFile(nil)
	/*
		excellogger.SetCellFontHeader()
		excellogger.WiteCellln("Introduction")
		excellogger.WiteCellln("Please Do not edit this page!")
		//excellogger.WiteCellln("This page is created by the User Report script: " + "https://git.aa.st/perolo/jira-utils/CustomfieldReport")
		t := time.Now()

		excellogger.WiteCellln("Created by: " + "perolo" + " : " + t.Format(time.RFC3339))
		excellogger.WiteCellln("")
		excellogger.WiteCellln(fmt.Sprintf("targetTemp %f.2", targetTemp))

		excellogger.SetCellFontHeader2()
		excellogger.WiteCellln("SimpleSun")
	*/
	headers = append(headers, "Time")
	headers = append(headers, "Currentpower")
	headers = append(headers, "OldTemp")
	headers = append(headers, "NewTemp")
	headers = append(headers, "DiffTemp")
	headers = append(headers, "CurrentFlow")
	headers = append(headers, "DeltaEnergy")

	excellogger.AutoFilterStart()
	excellogger.SetRowHeight(20)
	excellogger.WriteColumnsHeaderln(headers)
	diffTemp := 0.0
	for {

		// Get Current power
		currentPower = lininterp1d(timeser, lutpower, float64(theTime.Hour())+float64(theTime.Minute())/60)
		//currentPower = 50
		if currentPower != 0 {
			fmt.Printf("CurrentPower: %.0f \n", currentPower)
		}
		// Get new temp from Panel = energy / volume
		if currentFlow != 0 {
			diffTemp = (currentPower * area) / (waterspec * currentFlow)
		} else {
			//TODO Start pump if temperature measured is above targetTemp

			// Fake calculation to start pump
			diffTemp = (currentPower * area) / (waterspec * (0.1 / 60.0))
		}
		newTemp = oldTemp + diffTemp
		// If temperature reaches target adjust flow

		//Sanity check..
		deltaEnergy = waterspec * diffTemp * (currentFlow * (timeslice.Seconds() + 60*timeslice.Minutes()))

		//TODO Add some hysteresis
		if newTemp > targetTemp {
			newTemp = heatexchanger(currentFlow, newTemp, -radiatorflow, radiatorReturnTemp)
		}
		if currentFlow < 60 {
			if diffTemp > targetTemp-radiatorReturnTemp {
				// Very simple P regulator
				//currentFlow = currentFlow * 1.2
				//				currentFlow = 0.7 / 60.0
				adjFlow = currentPower * area / (waterspec * targetTemp)
				currentFlow = adjFlow
			}
		}
		if currentFlow > .1/60 {
			if diffTemp < targetTemp-radiatorReturnTemp {
				// Very simple P regulator
				//currentFlow = 0.7 / 60.0
				adjFlow = currentPower * area / (waterspec * targetTemp)
				currentFlow = adjFlow
				//				currentFlow = currentFlow / 1.2
			}
		}
		ddd := float64(time.Hour / timeslice)
		fmt.Printf("NewTemp: %.2f diffTemp: %.2f Flow:%.3f AdjFlow: %.2f Power: %.2f \n", newTemp, diffTemp, currentFlow*60, adjFlow*60, ddd*deltaEnergy/(1000*3600))

		// TODO Bug?: Works OK when writing to Excel, but libreoffice interprets as strins
		// TODO Use Google seets instead?
		writeLine(theTime, []float64{currentPower, oldTemp, newTemp, diffTemp, currentFlow * 60, ddd * deltaEnergy / (1000 * 3600)})

		theTime = theTime.Add(timeslice)
		if theTime.After(end) {
			break
		}
		oldTemp = newTemp
	}

	excellogger.SetAutoColWidth()
	excellogger.AutoFilterEnd()

	// Save xlsx file by the given path.
	excellogger.SaveAs(file)

}
func heatexchanger(primFlow float64, primTemp float64, SecFlow float64, secTemp float64) (outTemp float64) {

	var denom float64
	if primFlow == SecFlow {
		denom = primFlow
	} else {
		denom = primFlow - SecFlow
	}
	if primTemp == secTemp {
		outTemp = primTemp
	} else {
		// Simplified Assuming 100% efficiency and infinite power
		outTemp = (primFlow*primTemp - SecFlow*secTemp) / denom
	}
	return outTemp
}

func writeLine(t time.Time, o []float64) {
	row := []string{t.Format("15:04:05")}
	for i := range o {
		row = append(row, fmt.Sprintf("%.3f", o[i]))
	}
	excellogger.WriteColumnsln(row)
}
