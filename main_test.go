package main

import (
	"testing"
)

var test_timeser = []float64{0, 1, 4, 6, 8, 9, 10}
var test_lutser = []float64{0, 120, 500, 600, 500, 120, 0}

func Test_Interp(t *testing.T) {

	boundrylow := lininterp1d(test_timeser, test_lutser, 0)
	if boundrylow != 0.0 {
		t.Errorf("boundrylow(0) = %f; want 0.0", boundrylow)
	}

	outsidelow := lininterp1d(test_timeser, test_lutser, -1)
	if outsidelow != 0.0 {
		t.Errorf("outsidelow(0) = %f; want 0.0", outsidelow)
	}

	interpol := lininterp1d(test_timeser, test_lutser, 5.5)
	if interpol != 575 {
		t.Errorf("interpol(0) = %f; want 575", interpol)
	}

	spoton := lininterp1d(test_timeser, test_lutser, 9)
	if spoton != 120 {
		t.Errorf("spoton(0) = %f; want 120", spoton)
	}

	outsidehigh := lininterp1d(test_timeser, test_lutser, 45.5)
	if outsidehigh != 0.0 {
		t.Errorf("outsidehigh(0) = %f; want 0.0", outsidehigh)
	}

}

//heatexchanger(primFlow float64, primTemp float64, SecFlow float64, secTemp float64) (float64) {
func Test_HeatExchanger(t *testing.T) {

	equal := heatexchanger(10, 20, 10, 20)
	if equal != 20.0 {
		t.Errorf("equal(0) = %f; want 0.0", equal)
	}

	average := heatexchanger(10, 30, -10, 20)
	if average != 25.0 {
		t.Errorf("average(0) = %f; want 0.0", average)
	}

	smaller := heatexchanger(10, 30, -20, 20)
	if smaller > average {
		t.Errorf("smaller(0) = %f; want 0.0", smaller)
	}

	larger := heatexchanger(20, 30, -10, 20)
	if larger < average {
		t.Errorf("larger(0) = %f; want 0.0", larger)
	}

}
